function layThongTinTuForm() {
  // khai báo từng phần tử của object trog mảng dssv
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var diemToan = document.getElementById("txtDiemToan").value;
  var diemLy = document.getElementById("txtDiemLy").value;
  var diemHoa = document.getElementById("txtDiemHoa").value;

  var sv = {
    ma: ma,
    ten: ten,
    email: email,
    matKhau: matKhau,
    diemToan: diemToan,
    diemLy: diemLy,
    diemHoa: diemHoa,
  };
  return sv;
  // return new SinhVien(ma, ten, email, matKhau, diemToan, diemLy, diemHoa);
}

function renderDSSV(dssv) {
  var contenHTML = "";
  for (var i = 0; i < dssv.length; i++) {
    var item = dssv[i];
    var contentTr = `
        <tr>
          <td>${item.ma}</td>
          <td>${item.ten}</td>
          <td>${item.email}</td>
          <td>0</td>
          <td onclick="xoaSV(${item.ma})" class="btn btn-success">Xóa</td>
          <td onclick="suaSV(${item.ma})" class="btn btn-warning">Sửa</td>
        </tr>
        `;
    contenHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contenHTML;
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}
function resetForm() {
  document.getElementById("formQLSV").reset();
}

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
