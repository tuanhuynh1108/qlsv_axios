const BASE_URL = "https://63f442b9fe3b595e2ef03989.mockapi.io";

function fetchDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      //   console.log("res", res);
      var dssv = res.data;
      console.log("dssv: ", dssv);
      tatLoading();
      renderDSSV(dssv);
    })
    .catch(function (err) {
      console.log("err: ", err);
      tatLoading();
    });
}
fetchDSSV();

// logic: lấy thông tin từ form => đẩy lên API => renderDSSV
function themSV() {
  let sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      fetchDSSV();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
  resetForm();
  s;
}

function xoaSV(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchDSSV();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
function suaSV(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
function capNhat() {
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      fetchDSSV();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
  resetForm();
}
